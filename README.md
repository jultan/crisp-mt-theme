# Crisp 

A minimalist, responsive, and open-source theme for [Movable Type](https://movabletype.org) . Adapted from Ghost Theme by [Kathy Qian](http://kathyqian.com). You can [view it live here](http://kathyqian.com).

![Index](https://raw.github.com/jultan/crisp-mt-theme/master/index.png)   

### Required Steps for Installation

1. Download the files
2. Manually add/remove all links to static pages by copying (or deleting) the code in **crisp/partials/navigation.hbs**    
3. Replace the `example` disqus_shortname with your shortname on *line 13* of **crisp/post.hbs**, or delete the #comments div to remove comments altogether
4. Configure the follow buttons in **crisp/partials/follow.hbs** (see section below)
5. Add the "crisp" folder to the **content/themes** directory of your Ghost installation
6. Select the theme in the settings page of your MT admin panel

### Suggested Customizations

* Change the link color on *line 86* in **crisp/assets/styles/crisp.css**
* Add code for Google Analytics in **crisp/default.hbs** after `{{ghost_foot}}`
* Remove irrelevant social sharing services in **crisp/partials/share.hbs**
* Change your blog logo to change the favicon and the picture in the sidebar (the blog cover is not used)

### Editing Follow Buttons

Crisp uses Font Awesome for icons. See the Font Awesome documentation for the [full list of icons](http://fortawesome.github.io/Font-Awesome/icons/) and [usage tips](http://fortawesome.github.io/Font-Awesome/examples/). 

I have placed some common buttons in **follow.hbs**, with more options in the commented out sections. Make sure to replace the `username` in the URLs so the links point to your profiles. 

### Features, Changelog, and Technical Notes

This theme has been updated for Movable Type 6 and is compatible with all modern versions of Chrome, Firefox, Safari, and IE9+.

* Initial release - December 2014

### Credits

One big thanks to [Kathy Qian](http://kathyqian.com).

Many thanks to [@davegandy](http://twitter.com/davegandy) for the [Font Awesome](https://github.com/FortAwesome/Font-Awesome) icons used throughout the theme.

Social sharing buttons are a modified version of the [Ridiculously Responsive Social Sharing Buttons](https://github.com/kni-labs/rrssb) by [@dbox](http://www.twitter.com/dbox) and [@seagoat](http://www.twitter.com/seagoat). Great job, guys!

### License

This theme is licensed under the [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International](http://creativecommons.org/licenses/by-nc-sa/4.0/) license. Go crazy on the customizations, distribute to your friends, just give me some credit and don't sell my work. Feel free to modify the footer text, though I would really appreciate it if you could keep at least one of the links intact.

### More Screenshots

![Post](https://raw.github.com/jultan/crisp-mt-theme/master/post.png)
![Post w/Image](https://raw.github.com/jultan/crisp-mt-theme/master/post-2.png)
![Post w/Long Text](https://raw.github.com/jultan/crisp-mt-theme/master/post-3.png)
